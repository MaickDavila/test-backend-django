import os
from django.conf import settings
from django.db import models
from backend.utils import Utils


# Create your models here.
def path_and_rename(instance, filename):
    ext = filename.split(".")[-1]
    filename = "{}.{}".format(instance.uuid, ext)
    return os.path.join(settings.MEDIA_ROOT, "usuarios/", filename)


class Usuario(models.Model):
    uuid = models.CharField(default=Utils.generate_uuid, max_length=250)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.CharField(max_length=8)
    fecha_nacimiento = models.DateField()
    telefono = models.CharField(max_length=50, null=True, blank=True)
    direccion = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=50, unique=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    foto = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    SEXOS = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
    )
    sexo = models.CharField(max_length=1, choices=SEXOS)

    def save(self, *args, **kwargs):
        Utils.save_on_csv_file(self)
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.nombre} {self.apellido} - {self.email} - {self.direccion} - {self.telefono} - {self.fecha_nacimiento}"

    @staticmethod
    def nuevo_usuario(data, files):
        usuario = Usuario()
        usuario.nombre = data.get("nombre")
        usuario.apellido = data.get("apellido")
        usuario.dni = data.get("dni")
        usuario.fecha_nacimiento = data.get("fecha_nacimiento")
        usuario.telefono = data.get("telefono")
        usuario.direccion = data.get("direccion")
        usuario.email = data.get("email")
        usuario.sexo = data.get("sexo")
        errors = [] 

        if not usuario.nombre:
            errors.append("El nombre es obligatorio")
        if not usuario.apellido:
            errors.append("El apellido es obligatorio")
        if not usuario.dni:
            errors.append("El DNI es obligatorio")
        if not usuario.fecha_nacimiento:
            errors.append("La fecha de nacimiento es obligatoria")         
        if not usuario.email:
            errors.append("El email es obligatorio")
        if not usuario.sexo:
            errors.append("El sexo es obligatorio")

        if errors:
            return None, errors

        foto = files.get("foto")
        if foto:
            usuario.foto = foto                        
        usuario.save()         
        return usuario, None
    
    @staticmethod
    def modificar_usuario(data, files):         
        usuario = Usuario.get_by_uuid(data.get("uuid"))
        usuario.nombre = data.get("nombre")
        usuario.apellido = data.get("apellido")
        usuario.dni = data.get("dni")
        usuario.fecha_nacimiento = data.get("fecha_nacimiento")
        usuario.telefono = data.get("telefono")
        usuario.direccion = data.get("direccion")
        usuario.email = data.get("email")
        usuario.sexo = data.get("sexo")
        errors = []

        if not usuario.nombre:
            errors.append("El nombre es obligatorio")
        if not usuario.apellido:
            errors.append("El apellido es obligatorio")
        if not usuario.dni:
            errors.append("El DNI es obligatorio")
        if not usuario.fecha_nacimiento:
            errors.append("La fecha de nacimiento es obligatoria")
        if not usuario.email:
            errors.append("El email es obligatorio")
        if not usuario.sexo:
            errors.append("El sexo es obligatorio")

        if errors:
            return None, errors
            
        foto = files.get("foto")
        if foto:
            usuario.foto = foto         
        usuario.save() 
        return usuario, None

    # simple login
    @staticmethod
    def login(email):
        errors = []
        if not email:
            errors.append("Debe ingresar un email")

        if len(errors) > 0:
            return None, errors

        usuario = Usuario.objects.filter(email=email).first()
        if usuario:
            return usuario, errors

        errors.append("Email incorrecto")
        return None, errors
    
    @staticmethod
    def get_by_uuid(uuid):
        return Usuario.objects.filter(uuid=uuid).first()
