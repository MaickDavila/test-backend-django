from django.urls import path

from app.views import IndexView, RegisterUserView, ViewUser

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("registro/", RegisterUserView.as_view(), name="registro"),
    path("usuario/<uuid:uuid>/", ViewUser.as_view(), name="usuario-view"),
]
