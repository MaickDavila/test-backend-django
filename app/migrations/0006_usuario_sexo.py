# Generated by Django 3.2.13 on 2022-04-25 03:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20220424_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='sexo',
            field=models.CharField(choices=[('M', 'Masculino'), ('F', 'Femenino')], default=1, max_length=1),
            preserve_default=False,
        ),
    ]
