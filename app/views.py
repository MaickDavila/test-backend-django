import os
from django.conf import settings
from django.shortcuts import redirect, render
from django.views import View
from app.models import Usuario
# Create your views here.


class IndexView(View):
    def get(self, request):
        return render(request, "index.html")

    def post(self, request):
        email = request.POST.get("email")

        usuario, errors = Usuario.login(email)

        if not usuario:
            return render(request, 'index.html', {'errors': errors})

        return redirect('usuario-view', uuid=usuario.uuid)


class ViewUser(View):
    def get(self, request, uuid):
        usuario = Usuario.get_by_uuid(uuid)
        csv_path = os.path.join(
            settings.MEDIA_URL, "csvs", f'{usuario.uuid}.csv')
        return render(request, "usuario-view.html", {'usuario': usuario, "csv_path": csv_path})

    def post(self, request, uuid):
        data = request.POST
        files = request.FILES
        usuario, errors = Usuario.modificar_usuario(data, files)

        if not usuario:
            usuario = Usuario.get_by_uuid(uuid)
            return render(request, 'usuario-view.html', {"usuario": usuario,  'errors': errors})

        return redirect('usuario-view', uuid=usuario.uuid)


class RegisterUserView(View):
    def get(self, request):
        return render(request, 'usuario-new.html')

    def post(self, request):
        data = request.POST
        files = request.FILES
        usuario, errors = Usuario.nuevo_usuario(data, files)
        if not usuario:
            return render(request, 'usuario-new.html', {'errors': errors})

        return redirect('usuario-view', uuid=usuario.uuid)
