import os
import uuid

from django.conf import settings


class Utils:
    @staticmethod
    def generate_uuid():
        return str(uuid.uuid4())

    def save_on_csv_file(self):
        if not os.path.exists(settings.CSV_FOLDER_PATH):
            os.makedirs(settings.CSV_FOLDER_PATH)

        path = os.path.join(settings.CSV_FOLDER_PATH, f'{self.uuid}.csv')
        with open(path, 'w') as f:             
            fields = [field.name for field in self._meta.fields]             
            f.write(','.join(fields) + '\n')             
            f.write(','.join(str(getattr(self, field)) for field in fields) + '\n')
    
    def read_csv(self):
        path = os.path.join(settings.CSV_FOLDER_PATH, f'{self.uuid}.csv')
        with open(path, 'r') as f:
            return f.read()         

        