# Test Backend Python - Django

Para realizar esta pueba debes tener conocimientos en: Python, Django, Css, Js, Bootstrap, git, gitlab

## Prueba

La prueba consistirá en crear un modelo de django (registro de usuario o registro de productos), la prueba se dividirá en una funcionalidad básica y en funcionaldidades adicionales que sumarán puntos a la evaluación.

Este repositorio conteien la estructura base, los cambios realizados deben subirse en un repositio propio (privado).


## 1. Funcionalidad básica
Se desea un formulario básico creado a partir de django, los datos se guardan en una base de datos sqlite y en archivo csv. 

- [ ] Modelo básico
- [ ] Choices

Los campos que tendrán el modelo no son relevantes.


## 2. Funcionalidad adicionales

Las siguientes funcionalidades se contruirán sobre la funcionalidad base.

### Formulario 
Un usuario podrá guardar los datos desde un formulario.

### Guardado
Los datos recibidos por los formularios se guardarán en una base de datos y / o en un archivo csv, cada archivo deberá tener una etiqueta distinta.

### Procesamiento 
Una función de Python, que permita realizar una operación con los datos que contiene el archivo csv, procesarlos y enviar una salida.

- [ ] Leer el archivos csv.
- [ ] Realizar una operación (a discreción del programador).
- [ ] Devolver una salida en la interfaz del usuario.


## Objetivo de la prueba
- Evaluar el conocimiento general del lenguaje Python.
- Evaluar el conocimiento en arquitectura web (MVC).
- Comprobar el conocimiento y comprensión de proyecto futuro.
- Evaluar organización de clases, funciones, modulos, entre otos.
